class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :article
      t.string :owner
      t.timestamps null: false
    end
  end
end
