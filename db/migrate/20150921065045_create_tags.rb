class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.belongs_to :blog, index: true
      t.belongs_to :category, index: true
      t.timestamps null: false
    end
  end
end
