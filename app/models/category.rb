class Category < ActiveRecord::Base
  has_many :tags
  has_many :blogs, through: :tags
end
