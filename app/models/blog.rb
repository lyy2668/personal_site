class Blog < ActiveRecord::Base
  has_many :tags
  has_many :categories, through: :tags
  belongs_to :archive

  def tag_list
    return self.categories.map {|category| category.name}.join(",")
  end

  def self.recent_posts
    return Blog.order(:id).last 3
  end


end
