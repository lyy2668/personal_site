class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_layout_variables

  def set_layout_variables
    @categories = Category.all
    @recent_posts = Blog.recent_posts
    @archives = Archive.all
  end
end
