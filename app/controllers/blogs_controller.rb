class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :edit, :update, :destroy]

  # GET /blogs
  # GET /blogs.json
  def index
    if params[:archive_id]
      @blogs = Archive.find(params[:archive_id]).blogs
    elsif params[:category_id]
      @blogs = Category.find(params[:category_id]).blogs
    else
      @blogs = Blog.all
    end

  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
    @blog = Blog.find params[:id]
    # respond_to do |format|
    #   format.html { render 'show' }
    #   format.json { render :json => @blog }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      @blog = Blog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
      params[:blog]
    end
end
